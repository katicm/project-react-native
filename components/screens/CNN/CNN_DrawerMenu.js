import React from "react";
import {
  createMaterialTopTabNavigator,
  createStackNavigator
} from "react-navigation";
import CNN_Primary from "./CNN_Primary";
import CNN_Secondary from "./CNN_Secondary";
import Ionicons from "react-native-vector-icons/Ionicons";
import WebViewPage from "./WebViewPage";

const PrimaryRootNavigation = createMaterialTopTabNavigator(
  {
    CNN_Primary: {
      screen: CNN_Primary,
      navigationOptions: {
        tabBarLabel: "Top Trending",
        tabBarIcon: ({ tintColor }) => (
          <Ionicons name="md-trending-up" size={25} color={tintColor} />
        )
      }
    },
    CNN_Secondary: {
      screen: CNN_Secondary,
      navigationOptions: {
        tabBarLabel: "Recent News",
        tabBarIcon: ({ tintColor }) => (
          <Ionicons name="md-time" size={25} color={tintColor} />
        )
      }
    }
  },
  {
    tabBarPosition: "bottom",
    tabBarOptions: {
      showIcon: true,
      showLabel: true,
      activeTintColor: "#cc0000",
      inactiveTintColor: "white",
      pressColor: "#cc0000",
      indicatorStyle: { backgroundColor: "#cc0000" },
      labelStyle: {
        fontSize: 10
      },
      tabStyle: {
        height: 55
      },
      style: { backgroundColor: "#0e1111" }
    }
  }
);

export default createStackNavigator(
  { PrimaryRootNavigation, WebViewPage },
  { headerMode: "none" }
);
