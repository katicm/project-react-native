import React from "react";
import { Header, Left, Button, Body } from "native-base";
import { Image } from "react-native";
import Ionicons from "react-native-vector-icons/Ionicons";

const CNN_Header = props => (
  <Header androidStatusBarColor="black" style={{ backgroundColor: "#cc0000" }}>
    <Left>
      <Button transparent>
        {props.withDrawer ? (
          <Ionicons
            name="md-menu"
            onPress={() => props.navigation.openDrawer()}
            size={35}
          />
        ) : (
          <Ionicons
            name="md-arrow-round-back"
            onPress={() => props.navigation.pop()}
            size={35}
          />
        )}
      </Button>
    </Left>
    <Body>
      <Image
        style={{
          height: 35,
          width: 70,
          marginLeft: 40
        }}
        source={require("../../media/cnn_logo1.png")}
      />
    </Body>
  </Header>
);
export default CNN_Header;
