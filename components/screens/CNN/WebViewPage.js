import React from "react";
import { View, WebView } from "react-native";
import CNN_Header from "./CNN_Header";

const WebViewPage = props => (
  <View style={{ flex: 1 }}>
    <CNN_Header navigation={props.navigation} withDrawer={false} />
    <WebView
      source={{
        uri: props.navigation.getParam("url", "https://edition.cnn.com/")
      }}
      style={{ marginTop: 10 }}
    />
  </View>
);
export default WebViewPage;
