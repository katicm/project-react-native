import React from "react";
import { Header, Left, Button, Body } from "native-base";
import { Image } from "react-native";
import Ionicons from "react-native-vector-icons/Ionicons";

const BR_Header = props => (
  <Header androidStatusBarColor="black" style={{ backgroundColor: "black" }}>
    <Left>
      <Button transparent>
        {props.withDrawer ? (
          <Ionicons
            name="md-menu"
            onPress={() => props.navigation.openDrawer()}
            color="white"
            size={35}
          />
        ) : (
          <Ionicons
            name="md-arrow-round-back"
            onPress={() => props.navigation.pop()}
            color="white"
            size={35}
          />
        )}
      </Button>
    </Left>
    <Body>
      <Image
        style={{
          height: 35,
          width: 109,
          marginLeft: 30
        }}
        source={require("../../media/breport_logo1.png")}
      />
    </Body>
  </Header>
);
export default BR_Header;
