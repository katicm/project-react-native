import React from "react";
import { View, WebView } from "react-native";
import BR_Header from "./BR_Header";

const WebViewPage = props => (
  <View style={{ flex: 1 }}>
    <BR_Header navigation={props.navigation} withDrawer={false} />
    <WebView
      source={{
        uri: props.navigation.getParam("url", "https://bleacherreport.com/")
      }}
      style={{ marginTop: 10 }}
    />
  </View>
);
export default WebViewPage;
