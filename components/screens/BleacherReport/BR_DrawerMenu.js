import React from "react";
import {
  createMaterialTopTabNavigator,
  createStackNavigator
} from "react-navigation";
import BR_Primary from "./BR_Primary";
import BR_Secondary from "./BR_Secondary";
import Ionicons from "react-native-vector-icons/Ionicons";
import WebViewPage from "./WebViewPage";

const PrimaryRootNavigation = createMaterialTopTabNavigator(
  {
    BR_Primary: {
      screen: BR_Primary,
      navigationOptions: {
        tabBarLabel: "Top Trending",
        tabBarIcon: ({ tintColor }) => (
          <Ionicons name="md-trending-up" size={25} color={tintColor} />
        )
      }
    },
    BR_Secondary: {
      screen: BR_Secondary,
      navigationOptions: {
        tabBarLabel: "Recent News",
        tabBarIcon: ({ tintColor }) => (
          <Ionicons name="md-time" size={25} color={tintColor} />
        )
      }
    }
  },
  {
    tabBarPosition: "bottom",
    tabBarOptions: {
      showIcon: true,
      showLabel: true,
      activeTintColor: "white",
      inactiveTintColor: "white",
      pressColor: "white",
      indicatorStyle: { backgroundColor: "white" },
      labelStyle: {
        fontSize: 10
      },
      tabStyle: {
        height: 55
      },
      style: { backgroundColor: "#0e1111" }
    }
  }
);

export default createStackNavigator(
  { PrimaryRootNavigation, WebViewPage },
  { headerMode: "none" }
);
