import React from "react";
import {
  createMaterialTopTabNavigator,
  createStackNavigator
} from "react-navigation";
import FOX_Primary from "./FOX_Primary";
import FOX_Secondary from "./FOX_Secondary";
import Ionicons from "react-native-vector-icons/Ionicons";
import WebViewPage from "./WebViewPage";

const PrimaryRootNavigation = createMaterialTopTabNavigator(
  {
    FOX_Primary: {
      screen: FOX_Primary,
      navigationOptions: {
        tabBarLabel: "Top Trending",
        tabBarIcon: ({ tintColor }) => (
          <Ionicons name="md-trending-up" size={25} color={tintColor} />
        )
      }
    },
    FOX_Secondary: {
      screen: FOX_Secondary,
      navigationOptions: {
        tabBarLabel: "Recent News",
        tabBarIcon: ({ tintColor }) => (
          <Ionicons name="md-time" size={25} color={tintColor} />
        )
      }
    }
  },
  {
    tabBarPosition: "bottom",
    tabBarOptions: {
      showIcon: true,
      showLabel: true,
      activeTintColor: "#002a5c",
      inactiveTintColor: "white",
      pressColor: "#002a5c",
      indicatorStyle: { backgroundColor: "#002a5c" },
      labelStyle: {
        fontSize: 10
      },
      tabStyle: {
        height: 55
      },
      style: { backgroundColor: "#0e1111" }
    }
  }
);
export default createStackNavigator(
  { PrimaryRootNavigation, WebViewPage },
  { headerMode: "none" }
);
