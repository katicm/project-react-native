import React from "react";
import { View, WebView } from "react-native";
import FOX_Header from "./FOX_Header";

const WebViewPage = props => (
  <View style={{ flex: 1 }}>
    <FOX_Header navigation={props.navigation} withDrawer={false} />
    <WebView
      source={{
        uri: props.navigation.getParam("url", "https://www.foxnews.com/")
      }}
      style={{ marginTop: 10 }}
    />
  </View>
);
export default WebViewPage;
