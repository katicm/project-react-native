import React from "react";
import { Header, Left, Button, Body } from "native-base";
import { Image } from "react-native";
import Ionicons from "react-native-vector-icons/Ionicons";

const FOX_Header = props => (
  <Header androidStatusBarColor="black" style={{ backgroundColor: "#002a5c" }}>
    <Left>
      <Button transparent>
        {props.withDrawer ? (
          <Ionicons
            name="md-menu"
            onPress={() => props.navigation.openDrawer()}
            color="white"
            size={35}
          />
        ) : (
          <Ionicons
            name="md-arrow-round-back"
            onPress={() => props.navigation.pop()}
            color="white"
            size={35}
          />
        )}
      </Button>
    </Left>
    <Body>
      <Image
        style={{
          height: 40,
          width: 56,
          marginLeft: 45
        }}
        source={require("../../media/fox_logo1.png")}
      />
    </Body>
  </Header>
);
export default FOX_Header;
