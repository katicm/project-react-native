import React from "react";
import { View, WebView } from "react-native";
import Verge_Header from "./Verge_Header";

const WebViewPage = props => (
  <View style={{ flex: 1 }}>
    <Verge_Header navigation={props.navigation} withDrawer={false} />
    <WebView
      source={{
        uri: props.navigation.getParam("url", "https://www.theverge.com/")
      }}
      style={{ marginTop: 10 }}
    />
  </View>
);
export default WebViewPage;
