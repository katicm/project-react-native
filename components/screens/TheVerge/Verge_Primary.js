import React, { Component } from "react";
import {
  StyleSheet,
  View,
  ScrollView,
  FlatList,
  RefreshControl
} from "react-native";
import { connect } from "react-redux";
import { fetchVergeTOP } from "../../redux/actions";
import Verge_Header from "./Verge_Header";
import CustomExpandable from "../InCommon/CustomExpandable";

class Verge_Primary extends Component {
  state = {
    refreshing: false
  };
  componentDidMount() {
    if (this.props.data.length == 0) {
      this.props.fetchVergeTOP();
    }
  }
  renderItem = ({ item, index }) => (
    <CustomExpandable
      news={item}
      index={index}
      navigation={this.props.navigation}
    />
  );
  handleRefresh = () => {
    this.props.fetchVergeTOP();
  };
  render() {
    return (
      <View>
        <Verge_Header navigation={this.props.navigation} withDrawer={true} />
        <ScrollView
          style={[styles.height, this.props.dark && styles.dark]}
          refreshControl={
            <RefreshControl
              refreshing={this.state.refreshing}
              onRefresh={this.handleRefresh}
            />
          }
        >
          <View style={{ flex: 1 }}>
            <FlatList
              data={this.props.data}
              renderItem={this.renderItem}
              keyExtractor={(item, index) => index.toString()}
            />
          </View>
        </ScrollView>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  height: {
    height: 570
  },
  dark: {
    backgroundColor: "#323335"
  }
});
const mapStateToProps = state => ({
  data: state.vergeTOP,
  dark: state.dark
});
export default connect(
  mapStateToProps,
  { fetchVergeTOP }
)(Verge_Primary);
