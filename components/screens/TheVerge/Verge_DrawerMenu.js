import React from "react";
import {
  createMaterialTopTabNavigator,
  createStackNavigator
} from "react-navigation";
import Verge_Primary from "./Verge_Primary";
import Verge_Secondary from "./Verge_Secondary";
import Ionicons from "react-native-vector-icons/Ionicons";
import WebViewPage from "./WebViewPage";

const PrimaryRootNavigation = createMaterialTopTabNavigator(
  {
    Verge_Primary: {
      screen: Verge_Primary,
      navigationOptions: {
        tabBarLabel: "Top Trending",
        tabBarIcon: ({ tintColor }) => (
          <Ionicons name="md-trending-up" size={25} color={tintColor} />
        )
      }
    },
    Verge_Secondary: {
      screen: Verge_Secondary,
      navigationOptions: {
        tabBarLabel: "Recent News",
        tabBarIcon: ({ tintColor }) => (
          <Ionicons name="md-time" size={25} color={tintColor} />
        )
      }
    }
  },
  {
    tabBarPosition: "bottom",
    tabBarOptions: {
      showIcon: true,
      showLabel: true,
      activeTintColor: "#EF0290",
      inactiveTintColor: "white",
      pressColor: "#EF0290",
      indicatorStyle: { backgroundColor: "#EF0290" },
      labelStyle: {
        fontSize: 10
      },
      tabStyle: {
        height: 55
      },
      style: { backgroundColor: "#0e1111" }
    }
  }
);

export default createStackNavigator(
  { PrimaryRootNavigation, WebViewPage },
  { headerMode: "none" }
);
