import React from "react";
import { View, WebView } from "react-native";
import BBCSport_Header from "./BBCSport_Header";

const WebViewPage = props => (
  <View style={{ flex: 1 }}>
    <BBCSport_Header navigation={props.navigation} withDrawer={false} />
    <WebView
      source={{
        uri: props.navigation.getParam("url", "https://www.bbc.co.uk/sport")
      }}
      style={{ marginTop: 10 }}
    />
  </View>
);
export default WebViewPage;
