import React from "react";
import { Header, Left, Button, Body } from "native-base";
import { Image } from "react-native";
import Ionicons from "react-native-vector-icons/Ionicons";

const BBCSport_Header = props => (
  <Header androidStatusBarColor="black" style={{ backgroundColor: "#FFE540" }}>
    <Left>
      <Button transparent>
        {props.withDrawer ? (
          <Ionicons
            name="md-menu"
            onPress={() => props.navigation.openDrawer()}
            size={35}
          />
        ) : (
          <Ionicons
            name="md-arrow-round-back"
            onPress={() => props.navigation.pop()}
            size={35}
          />
        )}
      </Button>
    </Left>
    <Body>
      <Image
        style={{
          height: 30,
          width: 173,
          marginLeft: 10
        }}
        source={require("../../media/bbcs_logo1.png")}
      />
    </Body>
  </Header>
);
export default BBCSport_Header;
