import React from "react";
import {
  createMaterialTopTabNavigator,
  createStackNavigator
} from "react-navigation";
import BBCSport_Primary from "./BBCSport_Primary";
import BBCSport_Secondary from "./BBCSport_Secondary";
import Ionicons from "react-native-vector-icons/Ionicons";
import WebViewPage from "./WebViewPage";

const PrimaryRootNavigation = createMaterialTopTabNavigator(
  {
    BBCSport_Primary: {
      screen: BBCSport_Primary,
      navigationOptions: {
        tabBarLabel: "Top Trending",
        tabBarIcon: ({ tintColor }) => (
          <Ionicons name="md-trending-up" size={25} color={tintColor} />
        )
      }
    },
    BBCSport_Secondary: {
      screen: BBCSport_Secondary,
      navigationOptions: {
        tabBarLabel: "Recent News",
        tabBarIcon: ({ tintColor }) => (
          <Ionicons name="md-time" size={25} color={tintColor} />
        )
      }
    }
  },
  {
    tabBarPosition: "bottom",
    tabBarOptions: {
      showIcon: true,
      showLabel: true,
      activeTintColor: "#FFE540",
      inactiveTintColor: "white",
      pressColor: "#FFE540",
      indicatorStyle: { backgroundColor: "#FFE540" },
      labelStyle: {
        fontSize: 10
      },
      tabStyle: {
        height: 55
      },
      style: { backgroundColor: "#0e1111" }
    }
  }
);

export default createStackNavigator(
  { PrimaryRootNavigation, WebViewPage },
  { headerMode: "none" }
);
