import React, { Component } from "react";
import {
  StyleSheet,
  View,
  ScrollView,
  FlatList,
  RefreshControl
} from "react-native";
import { connect } from "react-redux";
import { fetchBBCSport } from "../../redux/actions";
import BBCSport_Header from "./BBCSport_Header";
import CustomExpandable from "../InCommon/CustomExpandable";

class BBCSport_Secondary extends Component {
  state = {
    refreshing: false
  };
  componentDidMount() {
    if (this.props.data.length == 0) {
      this.props.fetchBBCSport();
    }
  }
  renderItem = ({ item, index }) => (
    <CustomExpandable
      news={item}
      index={index}
      navigation={this.props.navigation}
    />
  );
  handleRefresh = () => {
    this.props.fetchBBCSport();
  };
  render() {
    return (
      <View>
        <BBCSport_Header navigation={this.props.navigation} withDrawer={true} />
        <ScrollView
          style={[styles.height, this.props.dark && styles.dark]}
          refreshControl={
            <RefreshControl
              refreshing={this.state.refreshing}
              onRefresh={this.handleRefresh}
            />
          }
        >
          <View style={{ flex: 1 }}>
            <FlatList
              data={this.props.data}
              renderItem={this.renderItem}
              keyExtractor={(item, index) => index.toString()}
            />
          </View>
        </ScrollView>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  height: {
    height: 570
  },
  dark: {
    backgroundColor: "#323335"
  }
});
const mapStateToProps = state => ({
  data: state.bbcs,
  dark: state.dark
});
export default connect(
  mapStateToProps,
  { fetchBBCSport }
)(BBCSport_Secondary);
