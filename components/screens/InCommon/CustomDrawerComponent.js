import React from "react";
import { View, Image } from "react-native";
import CustomDrawerMenu from "../InCommon/CustomDrawerMenu";
import CustomDrawerItems from "../InCommon/CustomDrawerItems";

const CustomDrawerComponent = props => (
  <View>
    <View
      style={{
        height: 200,
        backgroundColor: "black",
        alignItems: "center",
        justifyContent: "center"
      }}
    >
      <Image
        style={{
          height: 40,
          width: 200
        }}
        source={require("../../media/fake_news.png")}
      />
    </View>
    <CustomDrawerItems navigation={props.navigation} />
    <CustomDrawerMenu navigation={props.navigation} />
  </View>
);
export default CustomDrawerComponent;
