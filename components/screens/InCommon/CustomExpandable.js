import React, { Component } from "react";
import {
  Text,
  View,
  StyleSheet,
  Image,
  TouchableHighlight
} from "react-native";
import { ListItem } from "react-native-elements";
import TouchableScale from "react-native-touchable-scale";
import moment from "moment";
import { connect } from "react-redux";

class CustomExpandable extends Component {
  state = {
    isSelected: false
  };
  handlePress = () => {
    this.setState((prevState, prevProps) => ({
      isSelected: !prevState.isSelected
    }));
  };
  openOnWeb = () => {
    this.props.navigation.navigate("WebViewPage", {
      url: this.props.news.url
    });
  };
  renderCollapsible = () => (
    <View>
      <View style={{ flexDirection: "column" }}>
        <Text
          style={[
            styles.text,
            this.props.dark ? styles.textWhite : styles.textBlack
          ]}
        >
          {this.props.news.description}
        </Text>
        <Image
          style={{ height: 200, marginTop: 5 }}
          source={{ uri: this.props.news.urlToImage }}
        />
        <Text
          style={[
            styles.text,
            this.props.dark ? styles.textWhite : styles.textBlack
          ]}
        >
          {this.props.news.content}
        </Text>
        <TouchableHighlight onPress={this.openOnWeb}>
          <View style={styles.button}>
            <Image
              style={styles.buttonImg}
              source={require("../../media/web.png")}
            />
            <Text style={styles.buttonText}>VIEW ON WEB</Text>
          </View>
        </TouchableHighlight>
      </View>
    </View>
  );

  render() {
    return (
      <View>
        <ListItem
          onPress={this.handlePress}
          component={TouchableScale}
          friction={90}
          tension={100}
          activeScale={0.95}
          containerStyle={{ paddingTop: 15, paddingBottom: 15 }}
          hideChevron={this.state.isSelected}
          title={
            <View>
              <Text
                style={[
                  styles.textHeader,
                  this.props.dark ? styles.textWhite : styles.textBlack
                ]}
              >
                {this.props.news.title}
              </Text>
              <Text style={this.props.dark && { color: "white" }}>
                {moment(this.props.news.publishedAt)
                  .startOf("minute")
                  .fromNow()}
              </Text>
            </View>
          }
          subtitleStyle={{ color: "white" }}
          subtitle={
            <View>{this.state.isSelected && this.renderCollapsible()}</View>
          }
        />
      </View>
    );
  }
}
const mapStateToProps = state => ({
  dark: state.dark
});
export default connect(mapStateToProps)(CustomExpandable);

const styles = StyleSheet.create({
  text: {
    paddingBottom: 10,
    paddingTop: 10,
    fontFamily: "Roboto",
    fontSize: 15,
    textAlign: "left"
  },
  textHeader: {
    fontSize: 20,
    fontWeight: "bold"
  },
  textBlack: {
    color: "black"
  },
  textWhite: {
    color: "white"
  },
  button: {
    height: 35,
    flexDirection: "row",
    backgroundColor: "#03A9F4"
  },
  buttonImg: {
    width: 28,
    height: 28,
    marginLeft: 14,
    alignSelf: "center"
  },
  buttonText: {
    alignSelf: "center",
    marginLeft: 91,
    fontWeight: "500",
    fontSize: 17,
    color: "white"
  }
});
