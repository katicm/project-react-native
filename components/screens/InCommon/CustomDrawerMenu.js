import React, { Component } from "react";
import { Text, View } from "react-native";
import { connect } from "react-redux";
import { CheckBox } from "react-native-elements";
import { darkModeSwitch } from "../../redux/actions";

class CustomDrawerMenu extends Component {
  handlePress = () => {
    this.props.darkModeSwitch(!this.props.dark);
    this.props.navigation.closeDrawer();
  };
  render() {
    return (
      <View>
        <CheckBox
          title="Dark Mode"
          onPress={this.handlePress}
          checked={this.props.dark}
        />
        <Text />
      </View>
    );
  }
}
const mapStateToProps = state => ({
  dark: state.dark
});
export default connect(
  mapStateToProps,
  { darkModeSwitch }
)(CustomDrawerMenu);
