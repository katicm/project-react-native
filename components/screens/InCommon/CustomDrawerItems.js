import React from "react";
import { View, Image, ScrollView, TouchableHighlight } from "react-native";

class CustomDrawerItems extends React.Component {
  state = { selectedSource: "CNN" };
  handleSwitch = source => {
    this.setState({ selectedSource: source });
    this.props.navigation.navigate(source);
    this.props.navigation.closeDrawer();
  };
  render() {
    return (
      <View>
        <ScrollView>
          <View style={{ flex: 1 }}>
            <TouchableHighlight onPress={() => this.handleSwitch("CNN")}>
              <View
                style={{
                  alignItems: "center",
                  justifyContent: "center",
                  backgroundColor:
                    this.state.selectedSource === "CNN" ? "#C0C0C0" : "#F0F0F0"
                }}
              >
                <Image
                  style={{
                    height: 35,
                    width: 70,
                    marginVertical: 8
                  }}
                  source={require("../../media/cnn_logo1.png")}
                />
              </View>
            </TouchableHighlight>
            <TouchableHighlight onPress={() => this.handleSwitch("BR")}>
              <View
                style={{
                  alignItems: "center",
                  justifyContent: "center",
                  backgroundColor:
                    this.state.selectedSource === "BR" ? "#C0C0C0" : "#F0F0F0"
                }}
              >
                <Image
                  style={{
                    height: 35,
                    width: 109,
                    marginVertical: 8
                  }}
                  source={require("../../media/breport_logo1.png")}
                />
              </View>
            </TouchableHighlight>
            <TouchableHighlight onPress={() => this.handleSwitch("FOXNEWS")}>
              <View
                style={{
                  alignItems: "center",
                  justifyContent: "center",
                  backgroundColor:
                    this.state.selectedSource === "FOXNEWS"
                      ? "#C0C0C0"
                      : "#F0F0F0"
                }}
              >
                <Image
                  style={{
                    height: 40,
                    width: 56,
                    marginVertical: 8
                  }}
                  source={require("../../media/fox_logo1.png")}
                />
              </View>
            </TouchableHighlight>
            <TouchableHighlight onPress={() => this.handleSwitch("BBCSport")}>
              <View
                style={{
                  alignItems: "center",
                  justifyContent: "center",
                  backgroundColor:
                    this.state.selectedSource === "BBCSport"
                      ? "#C0C0C0"
                      : "#F0F0F0"
                }}
              >
                <Image
                  style={{
                    height: 30,
                    width: 173,
                    marginVertical: 8
                  }}
                  source={require("../../media/bbcs_logo1.png")}
                />
              </View>
            </TouchableHighlight>
            <TouchableHighlight onPress={() => this.handleSwitch("Verge")}>
              <View
                style={{
                  alignItems: "center",
                  justifyContent: "center",
                  backgroundColor:
                    this.state.selectedSource === "Verge"
                      ? "#C0C0C0"
                      : "#F0F0F0"
                }}
              >
                <Image
                  style={{
                    height: 35,
                    width: 142,
                    marginVertical: 8
                  }}
                  source={require("../../media/verge_logo2.png")}
                />
              </View>
            </TouchableHighlight>
          </View>
        </ScrollView>
      </View>
    );
  }
}
export default CustomDrawerItems;
