import {
  CNN_FETCH,
  CNN_FETCHTOP,
  FOX_FETCH,
  FOX_FETCHTOP,
  BR_FETCH,
  BR_FETCHTOP,
  Verge_FETCH,
  Verge_FETCHTOP,
  BBCSport_FETCH,
  BBCSport_FETCHTOP,
  DARKMODE
} from "../redux/types";
import { combineReducers } from "redux";

const dataCNN = (state = [], action) => {
  switch (action.type) {
    case CNN_FETCH: {
      return action.data.articles;
    }
    default: {
      return state;
    }
  }
};
const dataCNNTOP = (state = [], action) => {
  switch (action.type) {
    case CNN_FETCHTOP: {
      return action.data.articles;
    }
    default: {
      return state;
    }
  }
};
const dataFOX = (state = [], action) => {
  switch (action.type) {
    case FOX_FETCH: {
      return action.data.articles;
    }
    default: {
      return state;
    }
  }
};
const dataFOXTOP = (state = [], action) => {
  switch (action.type) {
    case FOX_FETCHTOP: {
      return action.data.articles;
    }
    default: {
      return state;
    }
  }
};
const dataBR = (state = [], action) => {
  switch (action.type) {
    case BR_FETCH: {
      return action.data.articles;
    }
    default: {
      return state;
    }
  }
};
const dataBRTOP = (state = [], action) => {
  switch (action.type) {
    case BR_FETCHTOP: {
      return action.data.articles;
    }
    default: {
      return state;
    }
  }
};
const dataVerge = (state = [], action) => {
  switch (action.type) {
    case Verge_FETCH: {
      return action.data.articles;
    }
    default: {
      return state;
    }
  }
};
const dataVergeTOP = (state = [], action) => {
  switch (action.type) {
    case Verge_FETCHTOP: {
      return action.data.articles;
    }
    default: {
      return state;
    }
  }
};
const dataBBCSport = (state = [], action) => {
  switch (action.type) {
    case BBCSport_FETCH: {
      return action.data.articles;
    }
    default: {
      return state;
    }
  }
};
const dataBBCSportTOP = (state = [], action) => {
  switch (action.type) {
    case BBCSport_FETCHTOP: {
      return action.data.articles;
    }
    default: {
      return state;
    }
  }
};
const dark = (state = false, action) => {
  switch (action.type) {
    case DARKMODE: {
      return action.data;
    }
    default: {
      return state;
    }
  }
};
export const reducer = combineReducers({
  cnn: dataCNN,
  cnnTOP: dataCNNTOP,
  fox: dataFOX,
  foxTOP: dataFOXTOP,
  br: dataBR,
  brTOP: dataBRTOP,
  verge: dataVerge,
  vergeTOP: dataVergeTOP,
  bbcs: dataBBCSport,
  bbcsTOP: dataBBCSportTOP,
  dark: dark
});
