import {
  CNN_FETCH,
  CNN_FETCHTOP,
  FOX_FETCH,
  FOX_FETCHTOP,
  BR_FETCH,
  BR_FETCHTOP,
  Verge_FETCH,
  Verge_FETCHTOP,
  BBCSport_FETCH,
  BBCSport_FETCHTOP,
  DARKMODE
} from "../redux/types";
import Config from "react-native-config";

const CNN_FETCHED = data => ({
  type: CNN_FETCH,
  data
});
const CNN_FETCHEDTOP = data => ({
  type: CNN_FETCHTOP,
  data
});
const FOX_FETCHED = data => ({
  type: FOX_FETCH,
  data
});
const FOX_FETCHEDTOP = data => ({
  type: FOX_FETCHTOP,
  data
});
const BR_FETCHED = data => ({
  type: BR_FETCH,
  data
});
const BR_FETCHEDTOP = data => ({
  type: BR_FETCHTOP,
  data
});
const Verge_FETCHED = data => ({
  type: Verge_FETCH,
  data
});
const Verge_FETCHEDTOP = data => ({
  type: Verge_FETCHTOP,
  data
});
const BBCSport_FETCHED = data => ({
  type: BBCSport_FETCH,
  data
});
const BBCSport_FETCHEDTOP = data => ({
  type: BBCSport_FETCHTOP,
  data
});
const DARKMODE_SWITCH = data => ({
  type: DARKMODE,
  data
});
export const fetchCNN = () => dispatch => {
  fetch(
    "https://newsapi.org/v2/everything?sources=cnn&language=en&pageSize=50&apiKey=" +
      `${Config.NEWS_API_KEY}`
  )
    .then(response => response.json())
    .then(responseJson => {
      dispatch(CNN_FETCHED(responseJson));
    })
    .catch(error => {
      console.error(error);
    });
};
export const fetchCNNTOP = () => dispatch => {
  fetch(
    "https://newsapi.org/v2/top-headlines?sources=cnn&language=en&apiKey=" +
      `${Config.NEWS_API_KEY}`
  )
    .then(response => response.json())
    .then(responseJson => {
      dispatch(CNN_FETCHEDTOP(responseJson));
    })
    .catch(error => {
      console.error(error);
    });
};
export const fetchFOX = () => dispatch => {
  fetch(
    "https://newsapi.org/v2/everything?sources=fox-news&language=en&pageSize=50&apiKey=" +
      `${Config.NEWS_API_KEY}`
  )
    .then(response => response.json())
    .then(responseJson => {
      dispatch(FOX_FETCHED(responseJson));
    })
    .catch(error => {
      console.error(error);
    });
};
export const fetchFOXTOP = () => dispatch => {
  fetch(
    "https://newsapi.org/v2/top-headlines?sources=fox-news&language=en&apiKey=" +
      `${Config.NEWS_API_KEY}`
  )
    .then(response => response.json())
    .then(responseJson => {
      dispatch(FOX_FETCHEDTOP(responseJson));
    })
    .catch(error => {
      console.error(error);
    });
};
export const fetchBR = () => dispatch => {
  fetch(
    "https://newsapi.org/v2/everything?sources=bleacher-report&language=en&pageSize=50&apiKey=" +
      `${Config.NEWS_API_KEY}`
  )
    .then(response => response.json())
    .then(responseJson => {
      dispatch(BR_FETCHED(responseJson));
    })
    .catch(error => {
      console.error(error);
    });
};
export const fetchBRTOP = () => dispatch => {
  fetch(
    "https://newsapi.org/v2/top-headlines?sources=bleacher-report&language=en&apiKey=" +
      `${Config.NEWS_API_KEY}`
  )
    .then(response => response.json())
    .then(responseJson => {
      dispatch(BR_FETCHEDTOP(responseJson));
    })
    .catch(error => {
      console.error(error);
    });
};
export const fetchVerge = () => dispatch => {
  fetch(
    "https://newsapi.org/v2/everything?sources=the-verge&language=en&pageSize=50&apiKey=" +
      `${Config.NEWS_API_KEY}`
  )
    .then(response => response.json())
    .then(responseJson => {
      dispatch(Verge_FETCHED(responseJson));
    })
    .catch(error => {
      console.error(error);
    });
};
export const fetchVergeTOP = () => dispatch => {
  fetch(
    "https://newsapi.org/v2/top-headlines?sources=the-verge&language=en&apiKey=" +
      `${Config.NEWS_API_KEY}`
  )
    .then(response => response.json())
    .then(responseJson => {
      dispatch(Verge_FETCHEDTOP(responseJson));
    })
    .catch(error => {
      console.error(error);
    });
};
export const fetchBBCSport = () => dispatch => {
  fetch(
    "https://newsapi.org/v2/everything?sources=bbc-sport&language=en&pageSize=50&apiKey=" +
      `${Config.NEWS_API_KEY}`
  )
    .then(response => response.json())
    .then(responseJson => {
      dispatch(BBCSport_FETCHED(responseJson));
    })
    .catch(error => {
      console.error(error);
    });
};
export const fetchBBCSportTOP = () => dispatch => {
  fetch(
    "https://newsapi.org/v2/top-headlines?sources=bbc-sport&language=en&apiKey=" +
      `${Config.NEWS_API_KEY}`
  )
    .then(response => response.json())
    .then(responseJson => {
      dispatch(BBCSport_FETCHEDTOP(responseJson));
    })
    .catch(error => {
      console.error(error);
    });
};
export const darkModeSwitch = parametar => dispatch => {
  dispatch(DARKMODE_SWITCH(parametar));
};
