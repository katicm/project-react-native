import React, { Component } from "react";
import { createDrawerNavigator, createAppContainer } from "react-navigation";
import { Provider } from "react-redux";
import { store, persistor } from "./components/redux/store";
import { PersistGate } from "redux-persist/integration/react";
import CNN_DrawerMenu from "./components/screens/CNN/CNN_DrawerMenu";
import FOX_DrawerMenu from "./components/screens/FOX/FOX_DrawerMenu";
import BR_DrawerMenu from "./components/screens/BleacherReport/BR_DrawerMenu";
import Verge_DrawerMenu from "./components/screens/TheVerge/Verge_DrawerMenu";
import BBCSport_DrawerMenu from "./components/screens/BBCSport/BBCSport_DrawerMenu";
import CustomDrawerComponent from "./components/screens/InCommon/CustomDrawerComponent";

export default class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <PersistGate loading={null} persistor={persistor}>
          <RootRootNavigation />
        </PersistGate>
      </Provider>
    );
  }
}

const RootDrawerNavigation = createDrawerNavigator(
  {
    CNN: { screen: CNN_DrawerMenu },
    FOXNEWS: { screen: FOX_DrawerMenu },
    BR: { screen: BR_DrawerMenu },
    Verge: { screen: Verge_DrawerMenu },
    BBCSport: { screen: BBCSport_DrawerMenu }
  },
  {
    contentComponent: CustomDrawerComponent,
    drawerWidth: 200,
    initialRouteName: "CNN",
    contentOptions: {
      activeTintColor: "blue",
      inactiveTintColor: "black",
      itemStyle: {}
    }
  }
);
const RootRootNavigation = createAppContainer(RootDrawerNavigation); //new in 3.0 navigation
